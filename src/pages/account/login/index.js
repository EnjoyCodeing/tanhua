import React, { Component } from "react";
import { View, Text, Image, StatusBar, TouchableOpacity, StyleSheet } from "react-native";
import { pxToDp } from "../../../utils/stylesKits";
import { Input } from 'react-native-elements';
import validator from "../../../utils/validator";
import request from "../../../utils/request";
import { ACCOUNT_LOGIN } from "../../../utils/pathMap";
import THButton from "../../../components/THButton";
class Index extends Component 
{
  constructor(){
    super();
    console.log('login constructor');
  }

  state = {
    //手机号
    phoneNumber: '13767862345',
    phoneValid: true,
    showLogin: false, //default should be true
  }

  phoneNumberChangeText = (phoneNumber) => {
    this.setState({phoneNumber})
    console.log(phoneNumber);
  }

  renderLogin=()=>{
    const {phoneNumber, phoneValid} = this.state;
    return(
      <View style={{ padding: pxToDp(20) }}>
        <View>
          <Text style={{ fontSize: pxToDp(25), color: '#888', fontWeight: 'bold' }}>手机号登录注册</Text>
        </View>
        <View style={{ marginTop: pxToDp(30) }}>
          <Input
            placeholder='请输入手机号码'
            maxLength={11}
            keyboardType="phone-pad"
            returnKeyType="done"
            value={phoneNumber}
            errorMessage={phoneValid ? "" : "手机号码格式不正确"}
            inputStyle={{ color: "#333" }}
            onChangeText={this.phoneNumberChangeText}
            onSubmitEditing={this.phoneNumberSubmitEdit}
            leftIcon={{ type: 'font-awesome', name: 'phone', color: '#ccc', size: pxToDp(20) }}
          />
        </View>
        <View style={styles.btnSuper}>
          <THButton
            onPress={this.phoneNumberSubmitEdit}
            style={styles.btn}
            textStyle={styles.text}>获取验证码</THButton>
        </View>
      </View>
    )
  }

  renderVerify = () => {
    return (
      <View style={{ padding: pxToDp(20) }}>
        <Text style={[styles.text,{fontWeight:'bold'}]}>输入6位验证码</Text>
        <Text style={{marginTop: 20, fontSize: 17}}>{'已发送到：+86 ' + this.state.phoneNumber}</Text>
        <View style={styles.btnSuper}>
        <THButton 
        style={styles.btn}
        textStyle={styles.text}>重新获取</THButton>
        </View>
      </View>
    )
  }
  phoneNumberSubmitEdit = async () => {
    console.log("手机号码点击完成");
    const {phoneNumber} = this.state;
    const phoneValid = validator.validatePhone(phoneNumber)
    this.setState({phoneValid})
    if (!phoneValid) {
      return;
    }
    const res = await request.post(ACCOUNT_LOGIN, {phone:phoneNumber})
    console.log(res);
    if (res.code=="10000") {
      this.setState({showLogin:false})
    }
  }
  handleGetVCode = ()=> {
    console.log('点击获取短信验证码');
  }

  render() {
    const {showLogin} = this.state;
    return (
      <View>
        <StatusBar backgroundColor='red' translucent={true} />
        <Image style={{ width: '100%', height: pxToDp(200) }} source={require('../../../res/profileBackground.jpg')} />
        {showLogin?this.renderLogin():this.renderVerify()}
      </View>
    )
  }
}

const btnHeight = 60;
const styles = StyleSheet.create({
  btn: {
    borderRadius: btnHeight * 0.5,
    width: '85%'
  },
  btnSuper: {
    width:'100%', 
    height: btnHeight, 
    marginTop:20,
    alignItems: 'center'
  },
  text:{
    fontSize: 25,
  }
})
export default Index;
import React from "react";
import { ActivityIndicator } from "react-native";
import Toast from "teaset/components/Toast/Toast";
// import { Toast, Theme } from "teaset";

class YQToast extends Object {
  static customKey = null;

  static showCustom(text) {
    if (YQToast.customKey) return;
    console.log('YQToast showCustom');
    YQToast.customKey = Toast.show({
      text,
      icon: <ActivityIndicator size='large' color='white' />,
      position: 'center',
      duration: 1000000,
    });
  }

  static hideCustom() {
    if (!YQToast.customKey) return;
    Toast.hide(YQToast.customKey);
    YQToast.customKey = null;
  }
}
export default YQToast;
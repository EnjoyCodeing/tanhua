import axios from "axios";
import { BASE_URI } from "./pathMap";
import QYToast from './Toast';
const instance = axios.create({
  baseURL:BASE_URI
})

instance.interceptors.request.use(function(config) {
  console.log('before request');
  QYToast.showCustom('请求中');
  return config
}, function(error){
  return Promise.reject(error);
})

instance.interceptors.response.use(function(response) {
  console.log('receive response data');
  QYToast.hideCustom();
  return  response.data;
}, function(error){
  return Promise.reject(error);
})

export default {
  get:instance.get,
  post:instance.post
}
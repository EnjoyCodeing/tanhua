import React  from 'react';
import { Text,StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

class Index extends React.Component {

  static defaultProps = {
    style: {},
    textStyle: {}
  }
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={{flex:1, width: '100%', height:'100%', ...this.props.style}}>
        <LinearGradient 
        start={{x:0,y:0}} 
        end={{x:1, y:0}} 
        colors={['#9D70CF', '#F27489']} 
        style={styles.linearGradient}>
          <Text style={[styles.content, this.props.textStyle]}>{this.props.children}</Text>
        </LinearGradient>

      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
    margin: 10,
    backgroundColor:'transparent'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 30,
    justifyContent: 'center'
  },
})
export default Index;